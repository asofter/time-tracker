$(document).ready(function() {
    $(".select2").select2({
        allowClear: true
    });
});

function initUser(user_id) {
    $(document).on('click', '#generatePasswordButton', function(event) {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/account/manage/get-password/',
            dataType: 'json',
            success: function(res) {
                $('#editform-password').val(res);
            }
        });
    });
}
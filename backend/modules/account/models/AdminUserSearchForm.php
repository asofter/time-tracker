<?php
namespace backend\modules\account\models;

use common\models\User;
use yii\base\Model;
use Yii;

class AdminUserSearchForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'string']
        ];
    }

    public function formName() {
        return '';
    }

    public function attributeLabels() {
        return [

        ];
    }

    public function scenarios() {
        return [
            'default' => ['email']
        ];
    }
}

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Accounts management';
?>
<div class="row">
    <div class="col-lg-6">
        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['/account/manage/index']]); ?>
        <div class="row">
            <div class="col-lg-8">
                <?php echo Html::activeTextInput($model, 'email', ['placeholder' => 'Email', 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-4">
                <?php echo Html::submitButton('Enter', array('class' => 'btn btn-primary')); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-lg-6">
        <?= Html::a('Add', ['/account/manage/add'], ['class' => 'btn btn-success btn-lg pull-right']); ?>
    </div>
    <div class="col-lg-12">
        <h1><?php echo $this->title; ?></h1>
    </div>
</div><!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php
        if($provider->count > 0) {
            echo \yii\grid\GridView::widget(
                [
                    'dataProvider' => $provider,
                    'showOnEmpty' => false,
                    'columns' => [
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'id',
                            'format' => 'text'
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'email',
                            'format' => 'html',
                            'value' => function ($model, $index, $widget) {
                                if($model->email) {
                                    return Html::a($model->email, ['/account/manage/auth', 'id' => $model->id]);
                                } else {
                                    return Html::a("No Email", ['/account/manage/auth', 'id' => $model->id]);
                                }
                            }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'role',
                            'format' => 'html',
                            'value' => function ($model, $index, $widget) use($roles) {
                                    return $roles[$model->role];
                                }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'name',
                            'format' => 'html',
                            'value' => function ($model, $index, $widget) {
                                    return $model->firstname . ' ' . $model->lastname;
                                }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'status',
                            'format' => 'html',
                            'value' => function ($model, $index, $widget) use($statuses) {
                                    return $statuses[$model->status];
                                }
                        ],

                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'created_at',
                            'format' => 'html',
                            'value' => function ($model, $index, $widget) {
                                    return date("Y-m-d H:i:s", $model->created_at);
                                }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'updated_at',
                            'format' => 'html',
                            'value' => function ($model, $index, $widget) {
                                    return date("Y-m-d H:i:s", $model->updated_at);
                                }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'format' => 'html',
                            'label' => '',
                            'value' => function ($model, $index, $widget) {
                                    return Html::a('<i class="glyphicon glyphicon-edit"></i>', ['/account/manage/edit', 'id' => $model->id]);
                                }
                        ],
                    ]
                ]
            );
        }
        ?>
    </div>
</div><!-- /.row -->


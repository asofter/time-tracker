<?php
use yii\helpers\Html;
use common\models\UserRoles;

$this->title = 'Roles';
?>
<div class="page-header">
    <h1><?php echo Html::encode($this->title); ?></h1>
</div>

<div class="row">
    <div class="col-lg-12">
        <?= Html::a("Add new role", ['/account/roles/add'], ['class' => 'btn btn-lg btn-primary']); ?>
        <p>&nbsp;</p>
        <table class="table table-hover table-striped">
            <tr>
                <td>Name</td>
                <td>Access type</td>
                <td></td>
            </tr>
            <?php foreach ($userRoles as $model): ?>
                <tr>
                    <td>
                        <?php
                        echo Html::a(Html::encode($model['name']), array('/account/roles/edit', 'id' => $model['id']));
                        ?>
                    </td>
                    <td>
                        <?php
                        if($model['type'] == UserRoles::TYPE_ADMIN)
                            echo 'Admin';
                        else
                            echo 'User';
                        ?>
                    </td>
                    <td>
                        <?= Html::a(NULL, ['/account/roles/delete', 'id' => $model['id']], ['class'=>'glyphicon glyphicon-trash']); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    </div>
</div>

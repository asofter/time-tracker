<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use common\models\UserRoles;

$this->title = 'New role';
?>
<div class="page-header">
    <h1><?php echo Html::encode($this->title); ?></h1>
</div>

<div class="row">
    <div class="col-lg-5">
        <?php
        $form = ActiveForm::begin(array('options' => array('id' => 'form-signup')));
        echo $form->field($model, 'name')->textInput();
        echo $form->field($model, 'type')->dropDownList(array(
            UserRoles::TYPE_ADMIN => 'Admin',
            UserRoles::TYPE_USER => 'User'
        ));
        ?>

        <div class="form-actions">
            <?php echo Html::submitButton('Add', array('class' => 'btn btn-primary')); ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

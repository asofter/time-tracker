<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Tracker - Backend";
?>
<style>
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #eee;
    }

    .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin .checkbox {
        font-weight: normal;
    }
    .form-signin .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .form-signin .control-label {
        display: none;
    }
    .form-signin .hint-block {
        display: none;
    }
    .form-signin .help-block {
        display: none;
    }
    .form-signin .form-group {
        margin-bottom:0px;
    }
    .form-signin .form-control:focus {
        z-index: 2;
    }
    .form-signin input[type="text"] {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }
    .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
    .login{padding-top: 65px;}
    .center{float: none; margin-left: auto; margin-right: auto;}
</style>
<section>
    <div class="container login">
        <div class="row ">
            <div class="center col-lg-6 col-md-6 col-sm-12 well">
                <legend>Login in the backend</legend>
                <?php $form = ActiveForm::begin(array('id' => 'login-form', 'options' => array('class' => 'form-signin'))); ?>
                <?php echo $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->hint('')->label(''); ?>
                <?php echo $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->hint('')->label('');; ?>
                <?php echo $form->field($model, 'rememberMe')->checkbox(); ?>
                <?php echo Html::submitButton('Enter', array('class' => 'btn btn-lg btn-primary btn-block')); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <p class="text-center muted ">&copy; Copyright <?= date("Y"); ?> - Tracker</p>
</section>
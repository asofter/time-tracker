<?php
namespace backend\modules\account\controllers;

use common\controllers\BackendController;
use common\models\User;
use common\models\UserRoles;
use Yii;
use common\models\LoginForm;

class DefaultController extends BackendController
{
    public function behaviors()
    {
        return array(
            'access' => array(
                'class' => \yii\filters\AccessControl::className(),
                'rules' => array(
                    // allow authenticated users
                    array(
                        'allow' => true,
                        'actions' => array('login'),
                        'roles' => array('?')
                    ),
                    array(
                        'allow' => true,
                        'actions' => array('logout'),
                        'roles' => array('@')
                    ),
                    // deny all
                    array(
                        'allow' => false
                    )
                )
            )
        );
    }

    public function actionLogin()
    {
        $this->layout = 'login';
        $model = new LoginForm();
        if ($model->load($_POST) && $model->login()) {
            return Yii::$app->response->redirect(array('site/default/index'));
        } else {
            return $this->render('login', array('model' => $model));
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return Yii::$app->response->redirect(array('site/default/index'));
    }

}
<?php
namespace backend\modules\account\controllers;

use common\controllers\BackendController;
use common\models\UserRoles;
use Yii;

class RolesController extends BackendController
{
    public function behaviors()
    {
        return array(
            'access' => array(
                'class' => \yii\filters\AccessControl::className(),
                'rules' => array(
                    array(
                        'allow' => true,
                        'actions' => array('index', 'add', 'edit', 'delete'),
                        'roles' => UserRoles::getAdminRoles()
                    ),
                    // deny all
                    array(
                        'allow' => false
                    )
                )
            )
        );
    }

    public function actionDelete($id) {
        $model = UserRoles::findOne($id);
        $model->delete();

        Yii::$app->session->setFlash('success', "Role has successfully deleted.");

        return $this->redirect(['/account/roles/index']);
    }

    public function actionAdd() {
        $model = new UserRoles();
        $model->scenario = 'insert';
        if($model->load($_POST)) {
            if($model->save()) {
                Yii::$app->session->setFlash('success', "New role has been successfully added.");
                return $this->redirect('/account/roles/index/');
            } else {
                Yii::$app->session->setFlash('error', "There were some problem.");
            }
        }
        return $this->render('add', array('model' => $model));
    }

    public function actionEdit($id) {
        $model = UserRoles::findOne($id);
        $model->scenario = 'update';
        if($model->load($_POST)) {
            if($model->save()) {
                Yii::$app->session->setFlash('success', "Role has been successfully changed.");
                return $this->redirect('/account/roles/index/');
            } else {
                Yii::$app->session->setFlash('error', "There were some problem.");
            }
        }
        return $this->render('edit', array('model' => $model));
    }

    public function actionIndex()
    {
        $userRoles = UserRoles::find()->asArray()->all();

        return $this->render('index', array('userRoles' => $userRoles));
    }

}
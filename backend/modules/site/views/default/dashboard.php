<?php
use yii\helpers\Html;
use \yii\helpers\Url;
$this->title = 'Dashboard';
?>

<h1><?php echo $this->title; ?></h1>

<div class="row">
    <div class="col-md-2">
        <?= Html::a('<i class="glyphicon glyphicon-remove"></i> Clear cache', ['/site/clear-cache/'],['class' => 'btn btn-primary btn-lg']); ?>
    </div>
</div>
<?php
namespace backend\modules\companies\models;

use common\models\User;
use yii\base\Model;
use Yii;

class AdminEmployeeSearchForm extends Model
{
    public $company;

    public function rules()
    {
        return [
            ['company', 'string']
        ];
    }

    public function formName() {
        return '';
    }

    public function attributeLabels() {
        return [

        ];
    }

    public function scenarios() {
        return [
            'default' => ['company']
        ];
    }
}

<?php
namespace backend\modules\companies\models;

use frontend\modules\company\models\Companies;
use yii\base\Model;
use Yii;

class CompanyForm extends Model
{
    public $id;
    public $name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'string', 'max' => 220],
            //['name', 'unique', 'targetClass' => Companies::className(), 'message' => 'This name has already been taken.'],
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            if($this->id) {
                $company = Companies::findOne($this->id);
            } else {
                $company = new Companies();
                $company->user_id = Yii::$app->getUser()->id;
                $company->status = Companies::STATUS_ACTIVE;
            }
            $company->attributes = $this->attributes;
            $company->save(false);

            return $company;
        }

        return null;
    }

    public function scenarios() {
        return [
            'default' => [
                'name'
            ],

        ];
    }

    public function attributeLabels() {
        return [
            'name' => "Name"
        ];
    }
}

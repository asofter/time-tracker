<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\LanguageHelper;

$this->title = "Employees";
?>

<div class="row">
    <div class="col-lg-6">
        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['/companies/employees/index']]); ?>
        <div class="row">
            <div class="col-lg-8">
                <?php echo Html::activeTextInput($model, 'company', ['placeholder' => 'Company name', 'class' => 'form-control']) ?>
            </div>
            <div class="col-lg-4">
                <?php echo Html::submitButton('Enter', array('class' => 'btn btn-primary')); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-lg-12">
        <h1><?= $this->title; ?></h1>
    </div>
</div><!-- /.row -->

<p>&nbsp;</p>

<div class="row">
    <div class="col-sm-12">
        <?php
        if($provider->count > 0) {
            echo \yii\grid\GridView::widget(
                [
                    'dataProvider' => $provider,
                    'showOnEmpty' => false,
                    'columns' => [
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'attribute' => 'id',
                            'format' => 'text'
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'format' => 'html',
                            'label' => 'User',
                            'value' => function ($model, $index, $widget) {
                                return Html::a($model->user->firstname.' '.$model->user->lastname, ['/account/manage/edit/', 'id' => $model->user->id]);
                            }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'format' => 'html',
                            'label' => 'Company',
                            'value' => function ($model, $index, $widget) {
                                return Html::a($model->company->name, ['/companies/edit/', 'id' => $model->company->id]);
                            }
                        ],
                        [
                            'class' => \yii\grid\DataColumn::className(),
                            'format' => 'html',
                            'label' => '',
                            'value' => function ($model, $index, $widget) {
                                return Html::a('<i class="glyphicon glyphicon-remove-circle"></i>', ['/companies/employees/delete/', 'id' => $model->id]);
                            }
                        ]
                    ]
                ]
            );
        } else {
            ?>
            <div class="alert alert-warning">You have not added any employees.</div>
        <?php
        }
        ?>
    </div>
</div><!-- /.row -->

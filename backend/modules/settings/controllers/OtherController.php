<?php
namespace backend\modules\settings\controllers;

use backend\modules\settings\models\OtherSettingsForm;
use backend\modules\settings\models\Settings;
use common\controllers\BackendController;
use common\models\UserRoles;
use Yii;

class OtherController extends BackendController
{
    public function behaviors()
    {
        return array(
            'access' => array(
                'class' => \yii\filters\AccessControl::className(),
                'rules' => array(
                    array(
                        'allow' => true,
                        'actions' => array('index'),
                        'roles' => UserRoles::getAdminRoles()
                    ),
                    // deny all
                    array(
                        'allow' => false
                    )
                )
            )
        );
    }

    public function actionIndex()
    {
        $formModel = new OtherSettingsForm();
        if ($formModel->load($_POST)) {
            foreach($formModel->getAttributes() as $name => $value) {
                $group = $formModel->getGroup($name);
                $setting = Settings::find()->andWhere(['name' => $name, 'group' => $group])->one();
                if($setting) {
                    $setting->value = $value;
                    $setting->scenario = 'update';
                    $setting->save(false);
                } else {
                    $setting = new Settings();
                    $setting->group = self::SETTING_GROUP;
                    $setting->name = $name;
                    $setting->value = $value;
                    $setting->scenario = 'insert';
                    $setting->save();
                }
            }
            Yii::$app->session->setFlash('success', "Settings have successfully saved.");
        } else {
            $settings = array();
            //$groups = $formModel->getGroups();
            $groups = [];
            foreach($groups as $group => $attributes) {
                $settings = array_merge($settings, Yii::$app->params[$group]);
            }
            $formModel->load(array('OtherSettingsForm' => $settings));
        }
        echo $this->render('index', array('formModel' => $formModel));
    }

}
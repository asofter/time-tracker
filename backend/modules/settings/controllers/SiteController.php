<?php
namespace backend\modules\settings\controllers;

use backend\modules\settings\models\MainSettingsForm;
use backend\modules\settings\models\Settings;
use common\controllers\BackendController;
use common\models\UserRoles;
use Yii;

class SiteController extends BackendController
{
    const SETTING_GROUP = 'main';

    public function behaviors()
    {
        return array(
            'access' => array(
                'class' => \yii\filters\AccessControl::className(),
                'rules' => array(
                    array(
                        'allow' => true,
                        'actions' => array('index'),
                        'roles' => UserRoles::getAdminRoles()
                    ),
                    // deny all
                    array(
                        'allow' => false
                    )
                )
            )
        );
    }

    public function actionIndex()
    {
        $formModel = new MainSettingsForm();
        if ($formModel->load($_POST)) {
            foreach($formModel->getAttributes() as $name => $value) {
                $setting = Settings::find()->andWhere(['name' => $name, 'group' => self::SETTING_GROUP])->one();
                if($setting) {
                    $setting->value = $value;
                    $setting->scenario = 'update';
                    $setting->save(false);
                } else {
                    $setting = new Settings();
                    $setting->group = self::SETTING_GROUP;
                    $setting->name = $name;
                    $setting->value = $value;
                    $setting->scenario = 'insert';
                    $setting->save();
                }
            }
            Yii::$app->session->setFlash('success', "Settings have successfully saved.");
        } else {
            $formModel->load(array($formModel->formName() => Yii::$app->params[self::SETTING_GROUP]));
        }
        echo $this->render('index', array('formModel' => $formModel));
    }

}
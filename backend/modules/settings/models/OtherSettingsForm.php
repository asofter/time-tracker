<?php
/**
 * Settings Form
 */

namespace backend\modules\settings\models;

use Yii;
use yii\base\Model;

class OtherSettingsForm extends Model
{
    /*
    public $dropboxAppKey;
    public $dropboxAppSecret;*/

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return array(
            array(array_keys($this->getAttributes()), 'required')
        );
    }

    public function getGroup($attribute) {
        foreach($this->groups as $group => $attributes) {
            if(in_array($attribute, $attributes))
                return $group;
        }

        return false;
    }

    public function getGroups() {
        return $this->groups;
    }

    public function attributeLabels() {
        return array(

        );
    }

}

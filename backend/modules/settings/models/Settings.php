<?php
namespace backend\modules\settings\models;

use Yii;
use yii\db\ActiveRecord;

class Settings extends ActiveRecord
{
    public function rules()
    {
        return [
            ['name', 'string'],
            [['group', 'name', 'value'], 'required', 'on' => 'insert'],
        ];
    }

    public function scenarios()
    {
        return [
            'update' => ['group', 'name', 'value'],
            'insert' => ['group', 'name', 'value']
        ];
    }

    public function attributeLabels()
    {
        return [
            'group' => 'Group',
            'name' => 'Name',
            'value' => 'Value'
        ];
    }
}

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/**
 * @var yii\web\View $this
 */
$this->title = 'Other Settings';
?>
<div class="row">
    <div class="col-lg-12">
        <h1><?php echo $this->title; ?></h1>
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(array('options' => array('id' => 'form-signup'))); ?>
        <h3></h3>
        <?php

        ?>
        <div class="form-actions">
            <?php echo Html::submitButton('Save', array('class' => 'btn btn-lg btn-primary')); ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div><!-- /.row -->


<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'System Settings';
?>

<h1><?php echo $this->title; ?></h1>

<?php
$form = ActiveForm::begin(array('options' => array('id' => 'form-signup')));
echo $form->field($formModel, 'siteName')->textInput();
echo $form->field($formModel, 'siteEmail')->textInput();
echo $form->field($formModel, 'adminEmail')->textInput();
echo $form->field($formModel, 'frontendUrl')->textInput();
echo $form->field($formModel, 'backendUrl')->textInput();
echo $form->field($formModel, 'siteStatus')->checkbox();
?>
<div class="form-actions">
    <?php echo Html::submitButton('Save', array('class' => 'btn btn-lg btn-primary')); ?>
</div>

<?php ActiveForm::end(); ?>

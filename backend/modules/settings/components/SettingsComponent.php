<?php
namespace backend\modules\settings\components;

use backend\modules\settings\models\Settings;
use Yii;
use yii\base\Component;

class SettingsComponent extends Component
{
    public function init() {
        $settings = Settings::find()->asArray()->all();

        foreach($settings as $setting) {
            Yii::$app->params[$setting['group']][$setting['name']] = $setting['value'];
        }
    }
}
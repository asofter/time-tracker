<?php
return [
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'baseUrl' => 'http://app.experitus.io/',
            'scriptUrl' => 'http://app.experitus.io/',
        ]
    ]
];

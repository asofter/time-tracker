<?php

use yii\db\Schema;
use yii\db\Migration;

class m151103_143807_company extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('companies', [
            'id'                   => 'pk',
            'user_id'              => Schema::TYPE_INTEGER . ' NOT NULL',
            'name'				   => Schema::TYPE_STRING . ' NOT NULL',
            'status'               => Schema::TYPE_SMALLINT. '(4) NOT NULL',
            'created_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);

        $this->createTable('company_employees', [
            'id'                   => 'pk',
            'user_id'              => Schema::TYPE_INTEGER . ' NOT NULL',
            'company_id'              => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('companies');
        $this->dropTable('company_employees');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

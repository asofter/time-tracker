<?php

use yii\db\Schema;
use yii\db\Migration;

class m151109_152020_time extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('tracker', [
            'id'                   => 'pk',
            'user_id'              => Schema::TYPE_INTEGER . ' NOT NULL',
            'date'                 => Schema::TYPE_DATE . ' NOT NULL',
            'company_id'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'is_manual'			   => 'tinyint(1) NOT NULL DEFAULT 0',
            'hours'                => Schema::TYPE_INTEGER . '(5) NOT NULL',
            'description'          => Schema::TYPE_TEXT . ' NOT NULL DEFAULT ""',
        ], $tableOptions);

        $this->createTable('tracker_log', [
            'id'                   => 'pk',
            'tracker_id'              => Schema::TYPE_INTEGER . ' NOT NULL',
            'time'              => Schema::TYPE_TIME . ' NOT NULL',
            'type'           => Schema::TYPE_SMALLINT . '(3) NOT NULL DEFAULT 0',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('tracker');
        $this->dropTable('tracker_log');
    }
}
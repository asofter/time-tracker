(function($) {
    "use strict";

    // Headhesive
    var options = {
        offset: 800,
        classes: {
            clone:   'banner--clone',
            stick:   'banner--stick',
            unstick: 'banner--unstick'
        }
    };

    // Initialise with options
    var banner = new Headhesive('.navbar', options);

    // Tabs
    $( "#horz_tabs" ).tabs();

    // Accordion
    $( "#accordion" ).accordion();

    // Page loading animation
    setTimeout(function() {
        $('body').addClass('loaded');
    }, 2000);

    // Wrap body content
    $("body").wrapInner( "<div class='wrapper'></div>");

    // Equal height pricing
    $('.price, .response > div').matchHeight();

    // Set button width inside pricing tables
    var width = $('.price').width();
    $('.price .btn').width(width - 60);

    $(window).resize(function() {
        var width = $('.price').width();
        $('.price .btn').width(width - 60);

    });

    // Replace logo
    $(".banner--clone .navbar-header a").replaceWith('<img src="/new_layout/images/logo-dark.png" alt="Tracker Logo">');

    // Animated dropdown menu
    $(".nav").bootstrapDropdownOnHover({
        mouseOutDelay: 100, // Number of milliseconds to wait before closing the menu on mouseleave
        responsiveThreshold: 992 // Pixel width where the menus should no-longer be activated by hover
    });
})(jQuery);
$(document).ready(function() {
    $(".select2").select2({
        allowClear: true
    });
});

function userSelected(item) {
    $('#user-id').val(item.id);
    $('#user-id').attr('data-email', item.email);
    $('.user-firstname').val(item.firstname);
    $('.user-lastname').val(item.lastname);
    $('.non-user').hide('fast');
    $('.userSelect').val(item.email);
}

function clearSelect(item,ui) {
    if($('#user-id').attr('data-email') != $(item).val()){
        $('#user-id').val('');
        $('#user-id').attr('data-email', '');
        $('.user-firstname').val('');
        $('.user-lastname').val('');
    }
    if($('#user-id').val() == '') {
        $('.non-user').show('fast');
    }
}
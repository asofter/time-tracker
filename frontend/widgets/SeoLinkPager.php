<?php
namespace frontend\widgets;

use Yii;
use yii\web\Link;
use yii\widgets\LinkPager;

class SeoLinkPager extends LinkPager {
    public $registerLinkTags = true;

    protected function registerLinkTags()
    {
        $view = $this->getView();
        foreach ($this->pagination->getLinks(true) as $rel => $href) {
            $view->registerLinkTag(['rel' => $rel, 'href' => $href], $rel);
        }
    }
}
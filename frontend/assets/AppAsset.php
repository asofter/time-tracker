<?php
namespace frontend\assets;

use yii\web\AssetBundle;


class AppAsset extends BaseAsset
{
    public $css = [
        'application/css/sb-admin.css',
        'application/css/site.css',
        'application/font-awesome/css/font-awesome.min.css',
        'application/css/select2.css',
        'application/css/plugins/metisMenu/metisMenu.min.css',
        'application/css/plugins/timeline.css'
    ];
    public $js = [
        'application/js/select2.min.js',
        'application/js/moment.min.js',
        'application/js/plugins/metisMenu/metisMenu.min.js',
        'application/js/sb-admin-2.js',
        'application/js/functions.js'
    ];
    public $depends = [
        '\yii\web\YiiAsset',
        '\yii\bootstrap\BootstrapAsset',
        '\yii\bootstrap\BootstrapPluginAsset',
        '\yii\jui\JuiAsset'
    ];
}

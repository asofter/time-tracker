<?php
namespace frontend\modules\site\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('modules/site', 'Name'),
            'subject' => Yii::t('modules/site', 'Subject'),
            'body' => Yii::t('modules/site', 'Message'),
            'email' => Yii::t('modules/site', 'E-mail'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        if($this->email) {
            return Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject('New message from ' . $this->name . ' - ' . $this->subject)
                ->setTextBody($this->body)
                ->send();
        }
        return true;
    }
}

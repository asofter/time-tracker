<?php
namespace frontend\modules\site\components;

use yii\helpers\Url;
use yii\web\UrlRule;
use Yii;

class MainUrlRule extends UrlRule
{
    public function init()
    {
        parent::init();
    }

    public function createUrl($manager, $route, $params)
    {
        return parent::createUrl($manager, $route, $params);
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        // Check if the last symbol is slash
        $requestExt = explode(".", $pathInfo);
        if(substr($pathInfo, -1) != "/" && count($requestExt) == 1) {
            Yii::$app->getResponse()->redirect(Url::to("/" . $pathInfo . "/"), 301);

            return false;
        }

        return false;
    }
}
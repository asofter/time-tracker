<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Technical support";

Yii::$app->params['page_title'] = ['title' => $this->title, 'description' => "Send message to administrators"];

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-6 col-md-7 col-sm-12">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Send message</h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['id' => 'contact-us', 'options' => ['role' => 'form']]); ?>
                <?php if(Yii::$app->getUser()->isGuest): ?>
                    <?= $form->field($model, 'name')->textInput() ?>
                <?php endif; ?>
                <?php if(Yii::$app->getUser()->isGuest || !Yii::$app->getUser()->getIdentity()->email): ?>
                    <?= $form->field($model, 'email')->textInput() ?>
                <?php endif; ?>

                <?= $form->field($model, 'subject')->textInput() ?>

                <?= $form->field($model, 'body')->textArea(['style' => 'height:200px;']) ?>

                <div class="form-group">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-info btn-single', 'name' => 'contact-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-5 col-sm-12">
        <div class="panel panel-color panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Contacts</h3>
            </div>
            <div class="panel-body">
                <ul class="list-group list-group-minimal">
                    <li class="list-group-item">
                        <span class="badge badge-roundless badge-white"><a href="mailto:hello@tourista.me">hello@tourista.me</a></span>
                        <i class="fa-inbox"></i>
                    </li>
                    <li class="list-group-item">
                        <span class="badge badge-roundless badge-white"><a href="https://www.facebook.com/tourista.me" target="_blank">tourista.me</a></span>
                        <i class="fa-facebook"></i>
                    </li>
                    <li class="list-group-item">
                        <span class="badge badge-roundless badge-white"><a href="https://twitter.com/touristahq" target="_blank">@touristahq</a></span>
                        <i class="fa-twitter"></i>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

\frontend\assets\LandingAsset::register($this);
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
<head>
    <meta http-equiv="content-type" content="text/html; charset=<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="/landing/images/favicon.ico" />

    <?php $this->head(); ?>

    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!--[if IE 9]>
    <link href="/landing/css/ie9.css" rel="stylesheet">
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php $this->beginBody() ?>

<!-- Homepage loading animation -->
<div id="loader-wrapper">
    <div class="spinner"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>

<?= $content; ?>

<div class="section footer no-margin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-5 copyright">
                <p>&copy; <a href="http://tracker.dev/">Tracker</a> <?= date("Y"); ?><br />
                    <a href="mailto:test@test.com">test@test.com</a></p>
            </div>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
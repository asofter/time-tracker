<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Sign Up';
?>

<section>
    <div class="container login">
        <div class="row ">
            <div class="center col-lg-6 col-md-6 col-sm-12 well">
                <legend>Sign Up</legend>
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'form-signin']]); ?>
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->hint('')->label(''); ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->hint('')->label(''); ?>
                <?= $form->field($model, 'firstname')->textInput(['placeholder' => 'First name'])->hint('')->label(''); ?>
                <?= $form->field($model, 'lastname')->textInput(['placeholder' => 'Last name'])->hint('')->label(''); ?>
                <p>&nbsp;</p>
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-block']); ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
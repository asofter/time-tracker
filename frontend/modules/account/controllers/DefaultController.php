<?php
namespace frontend\modules\account\controllers;

use common\controllers\FrontendController;
use frontend\modules\account\models\Integrations;
use frontend\modules\account\models\AccountForm;
use backend\modules\account\models\AdminAuth;
use frontend\modules\account\models\PasswordResetRequestForm;
use frontend\modules\account\models\ProfileForm;
use frontend\modules\account\models\ResetPasswordForm;
use frontend\modules\account\models\SignupForm;
use frontend\modules\account\models\UserActivations;
use frontend\modules\account\models\EmailForm;
use Yii;
use yii\authclient\clients\Twitter;
use yii\authclient\OAuth2;
use yii\base\InvalidParamException;
use yii\helpers\Json;
use yii\imagine\Image;
use yii\web\Response;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use common\models\User;
use common\models\LoginForm;
use yii\widgets\ActiveForm;

class DefaultController extends FrontendController
{
    protected static $model = null;

    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authSuccessCallback'],
            ],
        ];
    }

    public function behaviors()
    {
        return array(
            'access' => array(
                'class' => \yii\filters\AccessControl::className(),
                'rules' => array(
                    // allow authenticated users
                    array(
                        'allow' => true,
                        'actions' => array('forgot', 'reset-password'),
                        'roles' => array('?')
                    ),
                    array(
                        'allow' => true,
                        'actions' => array('edit'),
                        'roles' => array('@')
                    ),
                    array(
                        'allow' => true,
                        'actions' => array('logout', 'login', 'signup', 'auth', 'admin-auth', 'activation'),
                        'roles' => array('@', '?')
                    ),
                    // deny all
                    array(
                        'allow' => false
                    )
                )
            )
        );
    }

    public function actionAdminAuth($token) {
        $model = AdminAuth::findOne(['token' => $token]);
        if($model) {
            $user = User::findOne($model['user_id']);
            $model->delete();

            if(Yii::$app->user->login($user, 0)) {
                $this->goHome();
            } else {
                throw new HttpException(403);
            }
        } else {
            throw new HttpException(404);
        }
    }

    public function authSuccessCallback($client)
    {
        $attributes = $client->getUserAttributes();

        $authClient = Yii::$app->request->get('authclient');
        if($authClient == 'facebook') {
            $facebookId = $attributes['id'];
            $user = User::find()->where("email=:email OR facebook_id=:facebook_id", [':email' => $attributes['email'], ':facebook_id' => $facebookId])->one();
            if($user) {
                if($user['facebook_id'] == $facebookId) {
                    if(Yii::$app->getUser()->isGuest)
                        Yii::$app->getUser()->login($user, 365 * 86400);
                } elseif($user['facebook_id'] != $facebookId && $attributes['verified'] == 1) {
                    $user->facebook_id = $facebookId;
                    $user->save(false);
                    Yii::$app->getUser()->login($user, 365 * 86400);
                }
            } else {
                if(Yii::$app->getUser()->isGuest) {
                    $user = new User();
                    if(!$attributes['first_name']) {
                        $attributes['first_name'] = 'Facebook';
                    }
                    if(!$attributes['last_name']) {
                        $attributes['last_name'] = 'User';
                    }
                    $user->firstname = $attributes['first_name'];
                    $user->lastname = $attributes['last_name'];
                    $user->facebook_id = $facebookId;
                    $user->email = $attributes['email'];
                    $user->status = User::STATUS_ACTIVE;
                    $user->setPassword(Yii::$app->security->generateRandomString(8));
                    $user->generateAuthKey();
                    $user->save(false);

                    Yii::$app->getUser()->login($user, 365 * 86400);
                } else {
                    $user = Yii::$app->getUser()->getIdentity();
                    $user->facebook_id = $facebookId;
                    $user->save(false);
                }
            }
        } else {
            throw new HttpException(403);
        }
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $this->layout = 'login';

        $model = new LoginForm();
        if ($model->load($_POST) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionForgot()
    {
        $this->layout = 'login';

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('modules/account', 'Check your email for further instructions.'));
                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('modules/account', 'Sorry, we are unable to reset password for email provided.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        $this->layout = 'login';

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('modules/account', 'New password was saved.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionActivation($token) {
        $activation = UserActivations::find()->andWhere(['token' => $token])->one();
        if(!$activation) {
            throw new HttpException(404, Yii::t('modules/account', "No token found."));
        }

        $userModel = User::find()->andWhere(['id' => $activation->user_id])->one();
        if(!$userModel) {
            throw new HttpException(404, Yii::t('modules/account', "No user found."));
        }

        $userModel->status = User::STATUS_ACTIVE;
        $userModel->save(false);

        Yii::$app->mailer->compose('welcome', [])
            ->setFrom([\Yii::$app->params['main']['siteEmail'] => \Yii::$app->name])
            ->setTo($userModel->email)
            ->setSubject(Yii::t('modules/account', 'Welcome'))
            ->send();

        if (Yii::$app->getUser()->login($userModel)) {
            $activation->delete();

            return $this->goHome();
        } else {
            throw new HttpException(404);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionSignup() {
        if (!\Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $this->layout = 'login';

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $user = $model->signup();
            if ($user) {
                //Send activation message
                $token = Yii::$app->security->generateRandomString(10);
                $activation = new UserActivations();
                $activation->scenario = 'insert';
                $activation->user_id = $user->id;
                $activation->token = $token;
                $activation->save();
                Yii::$app->mailer->compose('emailVerification', ['user' => $user, 'token' => $token])
                    ->setFrom([\Yii::$app->params['main']['siteEmail'] => \Yii::$app->name])
                    ->setTo($user->email)
                    ->setSubject(Yii::t('modules/account', 'Please, verify your Email'))
                    ->send();

                Yii::$app->session->setFlash('success', Yii::t('modules/account', 'You have successfully registered and we sent you a message with activation link.'));

                return $this->goHome();
            }
        }

        return $this->render('signup', [
            'model' => $model
        ]);
    }

    public function actionEdit() {
        $user = Yii::$app->getUser()->getIdentity();

        $model = new ProfileForm();
        $model->id = Yii::$app->getUser()->getId();

        if ($model->load($_POST) && $model->validate()) {
            if(!empty($model->new_email) && $user->email == $model->new_email) {
                $model->new_email = '';
            }
            $model->save();
            Yii::$app->session->setFlash('success', Yii::t('modules/account', 'You have successfully changed your profile.'));
            return $this->refresh();
        } else {
            $model->attributes = $user->attributes;
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }
}

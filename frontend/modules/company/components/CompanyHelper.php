<?php
namespace frontend\modules\company\components;

use frontend\modules\company\models\Companies;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use Yii;
use common\components\CacheHelper;

class CompanyHelper extends Component {
    public static function getCompaniesByUseridAndId() {
        return CacheHelper::getFromModel(Companies::className(), ['user_id', 'id']);
    }
    
    public static function getCompanyByUseridAndId($userId, $id) {
        $modules = self::getCompaniesByUseridAndId();
        return isset($modules[$userId][$id]) ? $modules[$userId][$id] : false;
    }
    
    public static function getCompaniesById() {
        return CacheHelper::getFromModel(Companies::className(), 'id');
    }

    public static function getCompanyByUserId($userId) {
        $modules = self::getCompaniesByUseridAndId();
        if(isset($modules[$userId]) && count($modules[$userId]) > 0) {
            return reset($modules[$userId]);
        } else {
            $company = Companies::find()
                ->where(['status' => [Companies::STATUS_ACTIVE, Companies::STATUS_DISABLED]])
                ->joinWith(['employees' => function ($query) use($userId) {
                    $query->andWhere(['company_employees.user_id' => $userId]);
                }])->one();
            if($company) {
                return $company;
            }
        }

        return false;
    }

    public static function getCompanyById($id) {
        self::clearCompaniesCache();
        $companies = self::getCompaniesById();
        return isset($companies[$id]) ? $companies[$id] : false;
    }

    public static function clearCompaniesCache() {
        CacheHelper::clear(Companies::className(), ['user_id', 'id']);
        CacheHelper::clear(Companies::className(), 'id');
    }
}
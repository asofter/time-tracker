<?php
namespace frontend\modules\company\controllers;

use backend\modules\companies\models\CompanyForm;
use common\controllers\FrontendController;
use frontend\modules\company\models\Companies;
use frontend\modules\company\models\CompanyLogSearch;
use frontend\modules\company\models\Tracker;
use frontend\modules\company\models\TrackerDayForm;
use frontend\modules\company\models\TrackerLog;
use frontend\modules\company\models\TrackerStopDayForm;
use frontend\modules\site\components\SeoPagination;
use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\HttpException;
use frontend\modules\company\components\CompanyHelper;

class DefaultController extends FrontendController
{
    public function behaviors()
    {
        return array(
            'access' => array(
                'class' => \yii\filters\AccessControl::className(),
                'rules' => array(
                    // allow authenticated users
                    array(
                        'allow' => true,
                        'actions' => array('add', 'index', 'add-day', 'do', 'view', 'stop-day'),
                        'roles' => array('@')
                    ),
                    array(
                        'allow' => false
                    )
                )
            )
        );
    }


    public function actionAdd() {
        $company = $this->company;
        if($company) {
            return $this->redirect(['/']);
        }

    	$model = new CompanyForm();
        if ($model->load($_POST) && $model->validate()) {
            $company = $model->save();

            CompanyHelper::clearCompaniesCache();
            Yii::$app->session->setFlash('success', Yii::t('modules/company', "You have successfully added new company."));

            return $this->goHome();
        }

        return $this->render('add', [
            'model' => $model
        ]);
    }

    public function actionAddDay() {
        $company = $this->company;
        if(!$company || $company['user_id'] == Yii::$app->getUser()->getId()) {
            return $this->redirect(['/']);
        }

        $model = new TrackerDayForm();
        $model->start_time_hours = 9;
        $model->end_time_hours = 18;
        if ($model->load($_POST) && $model->validate()) {
            $model->save($company['id'], Yii::$app->getUser()->id);

            Yii::$app->session->setFlash('success', Yii::t('modules/company', "You have successfully added new date."));

            return $this->goHome();
        }

        $hours = range(0, 23);
        $minutes = range(0, 59);

        return $this->render('add_day', [
            'model' => $model,
            'hours' => $hours,
            'minutes' => $minutes
        ]);
    }

    public function actionDo($pause = null) {
        $company = $this->company;
        if(!$company || $company['user_id'] == Yii::$app->getUser()->getId()) {
            return $this->redirect(['/']);
        }

        $dayStatus = 1; // Start new day
        $trackerDay = Tracker::find()->andWhere(
            [
                'date' => date("Y-m-d"),
                'user_id' => Yii::$app->getUser()->id,
                'company_id' => $company['id'],
            ]
        )->asArray()->one();
        if($trackerDay) {
            $dayStatus = 3; // Stop day
            $lastRecordLog = TrackerLog::find()->andWhere(['tracker_id' => $trackerDay['id']])->orderBy(['id' => SORT_DESC])->asArray()->one();
            if($lastRecordLog) {
                if($lastRecordLog['type'] == TrackerLog::STOP_TYPE) {
                    $dayStatus = 5; // Can't do anything
                } elseif($lastRecordLog['type'] == TrackerLog::PAUSE_TYPE && $pause) {
                    $dayStatus = 2; // Resume day
                } elseif($pause) {
                    $dayStatus = 4; // Pause day
                }
            }
        }

        if($dayStatus == 1) {
            $trackerDay = new Tracker();
            $trackerDay->description = "";
            $trackerDay->hours = 0;
            $trackerDay->date = date("Y-m-d");
            $trackerDay->is_manual = 0;
            $trackerDay->user_id = Yii::$app->getUser()->id;
            $trackerDay->company_id = $company['id'];
            $result = $trackerDay->save(false);

            if($result) {
                $log = new TrackerLog();
                $log->tracker_id = $trackerDay['id'];
                $log->time = date("H:i:s");
                $log->type = TrackerLog::START_TYPE;
                $log->save(false);
            }
        } elseif($dayStatus == 4) {
            $log = new TrackerLog();
            $log->tracker_id = $trackerDay['id'];
            $log->time = date("H:i:s");
            $log->type = TrackerLog::PAUSE_TYPE;
            $log->save(false);
        } elseif($dayStatus == 2) {
            $log = new TrackerLog();
            $log->tracker_id = $trackerDay['id'];
            $log->time = date("H:i:s");
            $log->type = TrackerLog::START_TYPE;
            $log->save(false);
        } elseif($dayStatus == 3) {
            return $this->redirect(['/company/default/stop-day']);
        } else {
            throw new HttpException(404);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionStopDay() {
        $company = $this->company;
        if(!$company || $company['user_id'] == Yii::$app->getUser()->getId()) {
            return $this->redirect(['/']);
        }

        $trackerDay = Tracker::find()->andWhere(
            [
                'date' => date("Y-m-d"),
                'hours' => 0,
                'user_id' => Yii::$app->getUser()->id,
                'company_id' => $company['id'],
                'is_manual' => 0
            ]
        )->asArray()->one();
        if(!$trackerDay) {
            throw new HttpException(404);
        }

        $model = new TrackerStopDayForm();
        if ($model->load($_POST) && $model->validate()) {
            $model->save($trackerDay['id']);

            return $this->goHome();
        }

        return $this->render('stop_day', [
            'model' => $model
        ]);
    }

    public function actionIndex() {
        $company = $this->company;
        if(!$company) {
            return $this->redirect(['/company/default/add']);
        }

        if($company['user_id'] == Yii::$app->getUser()->getId()) {
            $filterModel = new CompanyLogSearch();
            $params = Yii::$app->request->get();
            $result = $filterModel->search($company->id, $params);
            $dataProvider = $result['data'];
            $dataProvider->pagination = new SeoPagination(['pageSize' => 40]);

            $avgPerDay = $result['avg_per_day'];
            $avgPerDay->limit($dataProvider->pagination->getLimit())->offset($dataProvider->pagination->getOffset());
            $avgPerDay = $avgPerDay->asArray()->all();

            return $this->render('index_owner', [
                'dataProvider' => $dataProvider,
                'filterModel' => $filterModel,
                'avgHours' => $result['avg_hours'],
                'totalHours' => $result['total_hours'],
                'avgPerDay' => $avgPerDay
            ]);
        } else {
            // Tracker day button
            $dayStatus = 1; // Start button show
            $trackerDay = Tracker::find()->andWhere(
                [
                    'date' => date("Y-m-d"),
                    'user_id' => Yii::$app->getUser()->id,
                    'company_id' => $company['id']
                ]
            )->asArray()->one();
            if($trackerDay) {
                $dayStatus = 3;
                $lastRecordLog = TrackerLog::find()->andWhere(['tracker_id' => $trackerDay['id']])->orderBy(['id' => SORT_DESC])->asArray()->one();
                if($lastRecordLog) {
                    if($lastRecordLog['type'] == TrackerLog::STOP_TYPE) {
                        $dayStatus = 4;
                    } elseif($lastRecordLog['type'] == TrackerLog::PAUSE_TYPE) {
                        $dayStatus = 2;
                    }
                }
            }

            // Get log of the employee
            $log = Tracker::find()->andWhere(['company_id' => $company['id'], 'user_id' => Yii::$app->getUser()->id])->orderBy(['date' => SORT_DESC]);
            $count = $log->count();
            $pagination = new SeoPagination(['totalCount' => $count, 'pageSize' => 20]);
            $log->offset($pagination->offset)->limit($pagination->limit);
            $log = $log->all();

            $rowData = Tracker::find()->select("AVG(hours) as avg_hours, SUM(hours) as total_hours")->andWhere(['company_id' => $company['id'], 'user_id' => Yii::$app->getUser()->id])->asArray()->one();
            $avgHours = round($rowData['avg_hours'], 1);
            $totalHours = round($rowData['total_hours'], 1);

            return $this->render('index_employee', [
                'dayStatus' => $dayStatus,
                'log' => $log,
                'avgHours' => $avgHours,
                'totalHours' => $totalHours
            ]);
        }
    }

    public function actionView($id) {
        $company = $this->company;
        if(!$company) {
            return $this->redirect(['/']);
        }

        if($company['user_id'] == Yii::$app->getUser()->getId()) {
            $tracker = Tracker::find()->andWhere(['id' => $id, 'company_id' => $company['id']])->one();
        } else {
            $tracker = Tracker::find()->andWhere(['id' => $id, 'company_id' => $company['id'], 'user_id' => Yii::$app->getUser()->id])->one();
        }

        if(!$tracker) {
            throw new HttpException(404);
        }

        return $this->render('view', ['tracker' => $tracker]);
    }
}
<?php
namespace frontend\modules\company\controllers;

use common\controllers\FrontendController;
use frontend\modules\company\models\CompanyEmployeeForm;
use frontend\modules\company\models\CompanyEmployees;
use frontend\modules\company\models\Tracker;
use Yii;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use frontend\modules\site\components\SeoPagination;
use common\models\User;
use yii\helpers\ArrayHelper;

class EmployeesController extends FrontendController
{
    const LIST_PAGE_SIZE = 15;

    public function behaviors()
    {
        return array(
            'access' => array(
                'class' => \yii\filters\AccessControl::className(),
                'rules' => array(
                    array(
                        'allow' => true,
                        'actions' => array('index','add','delete','get-user', 'view'),
                        'roles' => array('@')
                    ),
                    // deny all
                    array(
                        'allow' => false
                    )
                )
            )
        );
    }

    public function actionView($id) {
        if(!$this->company) {
            throw new HttpException(404);
        }

        $user = User::findOne($id);
        if(!$user) {
            throw new HttpException(404);
        }

        $tracker = Tracker::find()
            ->andWhere(['company_id' => $this->company->id, 'user_id' => $user['id']])
            ->orderBy(['date' => SORT_DESC, 'hours' => SORT_ASC])
            ->limit(30)
            ->all();


        $rowData = Tracker::find()->select("AVG(hours) as avg_hours, SUM(hours) as total_hours")->andWhere(['company_id' => $this->company->id, 'user_id' => $user['id']])->asArray()->one();
        $avgHours = round($rowData['avg_hours'], 1);
        $totalHours = round($rowData['total_hours'], 1);

        $avgPerDay = Tracker::find()
            ->select("date, AVG(hours) as avg_hours")
            ->andWhere(['company_id' => $this->company->id, 'user_id' => $user['id']])
            ->groupBy(['date'])
            ->asArray()->all();

        return $this->render('view', [
            'user' => $user,
            'tracker' => $tracker,
            'avgHours' => $avgHours,
            'totalHours' => $totalHours,
            'avgPerDay' => $avgPerDay
        ]);
    }

    public function actionIndex() {
        if(!$this->company) {
            throw new HttpException(404);
        }

        $employees = CompanyEmployees::find()->where(['company_id' => $this->company_id]);
        $allEmployeesCount = $employees->count();
        $pagination = new SeoPagination(['totalCount' => $allEmployeesCount, 'pageSize' => self::LIST_PAGE_SIZE]);
        $employees->offset($pagination->offset)->limit($pagination->limit);
        $allEmployees = $employees->all();

        return $this->render('index', [
            'employees' => $allEmployees,
            'companyId' => $this->company_id
        ]);
    }

    public function actionAdd() {
        if(!$this->company) {
            throw new HttpException(404);
        }
        $companyId = $this->company_id;

        $model = new CompanyEmployeeForm();

        if(isset($_POST[$model->formName()]) && $_POST[$model->formName()]['user_id'] == ""){
            $model->scenario = 'non-user';
        }

        if ($model->load($_POST)) {
            $model->company_id = $companyId;
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            }

            if ($model->validate()) {
                $model->save();

                Yii::$app->session->setFlash('success', Yii::t('modules/company', 'You have successfully added new employee.'));
                return $this->redirect(['/company/employees/index']);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'companyId' => $companyId
        ]);
    }

    public function actionDelete($id) {
        if(!$this->company) {
            throw new HttpException(404);
        }
        $companyId = $this->company_id;

        $employee = CompanyEmployees::find()->where(['id' => $id, 'company_id' => $companyId])->one();
        if(!$employee) {
            throw new HttpException(404);
        }

        $employee->delete();

        Yii::$app->session->setFlash('success', Yii::t('modules/company', 'You have successfully removed this employee.'));
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionGetUser($term) {
        if(!$this->company) {
            throw new HttpException(404);
        }
        $companyId = $this->company_id;
        
        if(!isset($term)) {
            throw new HttpException(403);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $term = Yii::$app->request->get('term');

        $result = [];
        $users = User::find()->where(['email' => $term])->andWhere('id != :id', [':id' => Yii::$app->getUser()->getId()])->andWhere('id NOT IN (SELECT user_id FROM company_employees WHERE company_id="'.$companyId.'")')->limit(5)->asArray()->all();
        if(!$users) {
            return [];
        }

        foreach ($users as $user) {
            $result[] = [
                'type' => 'user',
                'label' => $user['email'] . ' (' . $user['firstname']. ' ' .$user['lastname'] . ')',
                'firstname' => $user['firstname'],
                'lastname' => $user['lastname'],
                'email' => $user['email'],
                'id' => $user['id']
            ];
        }

        return $result;
    }
}
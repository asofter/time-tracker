<?php
namespace frontend\modules\company\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;

class TrackerLog extends ActiveRecord
{
    const START_TYPE = 1;
    const PAUSE_TYPE = 2;
    const STOP_TYPE = 3;

    public function getTracker()
    {
        return $this->hasOne(Tracker::className(), ['id' => 'tracker_id']);
    }

    public function rules()
    {
        return [
            [['tracker_id', 'time', 'type'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => ['tracker_id', 'time', 'type']
        ];
    }
}

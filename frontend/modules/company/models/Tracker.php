<?php
namespace frontend\modules\company\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;

class Tracker extends ActiveRecord
{
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    public function getLog()
    {
        return $this->hasMany(TrackerLog::className(), ['tracker_id' => 'id']);
    }

    public function rules()
    {
        return [
            [['user_id', 'company_id', 'hours', 'is_manual'], 'integer'],
            ['description', 'string'],
            ['is_manual', 'default', 'value' => 0]
        ];
    }

    public function scenarios()
    {
        return [
            'default' => [
                'user_id', 'company_id',
                'hours', 'is_manual',
                'description', 'date'
            ]
        ];
    }
}

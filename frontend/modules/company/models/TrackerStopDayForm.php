<?php
namespace frontend\modules\company\models;

use yii\base\Model;
use Yii;
use common\models\User;

class TrackerStopDayForm extends Model
{
    public $description;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['description', 'required']
        ];
    }

    public static function timeDiff($time1, $time2) {
        $t1 = StrToTime($time1);
        $t2 = StrToTime($time2);
        $diff = $t1 - $t2;
        $hours = $diff / ( 60 * 60 );

        return $hours;
    }

    public function save($trackerId) {
        // Add record to tracker
        $tracker = Tracker::findOne($trackerId);
        if(!$tracker)
            return false;
        $tracker->description = $this->description;

        // Add record to tracker log
        $log = new TrackerLog();
        $log->tracker_id = $trackerId;
        $log->time = date("H:i:s");
        $log->type = TrackerLog::STOP_TYPE;
        $log->save(false);

        // Calculate number of hours
        $hours = 0;
        $logs = TrackerLog::find()->andWhere(['tracker_id' => $trackerId])->orderBy(['time' =>SORT_ASC])->asArray()->all();
        $previousTime = '';
        foreach($logs as $log) {
            if($log['type'] == TrackerLog::STOP_TYPE) {
                $hours += self::timeDiff($log['time'], $previousTime);
            } elseif($log['type'] == TrackerLog::PAUSE_TYPE) {
                $hours += self::timeDiff($log['time'], $previousTime);
                $previousTime = $log['time'];
            } elseif($log['type'] == TrackerLog::START_TYPE) {
                $previousTime = $log['time'];
            }
        }

        $tracker->hours = round($hours);
        $result = $tracker->save(false);

        return $result;
    }

    public function scenarios() {
        return [
            'default' => [
                'description'
            ]
        ];
    }
}

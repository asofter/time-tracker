<?php
namespace frontend\modules\company\models;

use yii\base\Model;
use Yii;
use common\models\User;

class TrackerDayForm extends Model
{
    public $hours;
    public $description;
    public $start_time_hours;
    public $start_time_minutes;
    public $end_time_hours;
    public $end_time_minutes;
    public $date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hours', 'date', 'start_time', 'end_time'], 'required'],
            ['hours', 'integer']
        ];
    }

    public function save($companyId, $userId) {
        // Add record to tracker
        $tracker = new Tracker();
        $tracker->description = $this->description;
        $tracker->hours = $this->hours;
        $tracker->date = date("Y-m-d", strtotime($this->date));
        $tracker->is_manual = 1;
        $tracker->user_id = $userId;
        $tracker->company_id = $companyId;
        $result = $tracker->save(false);

        // Add record to tracker log
        $log = new TrackerLog();
        $log->tracker_id = $tracker['id'];
        $log->time = date("H:i:s", strtotime($this->start_time_hours . ":" . $this->start_time_minutes));
        $log->type = TrackerLog::START_TYPE;
        $log->save(false);

        $log = new TrackerLog();
        $log->tracker_id = $tracker['id'];
        $log->time = date("H:i:s", strtotime($this->end_time_hours . ":" . $this->end_time_minutes));
        $log->type = TrackerLog::STOP_TYPE;
        $log->save(false);

        return $result;
    }

    public function scenarios() {
        return [
            'default' => [
                'hours', 'description',
                'date',
                'start_time_hours', 'end_time_hours',
                'start_time_minutes', 'end_time_minutes'
            ]
        ];
    }

    public function attributeLabels() {
        return [

        ];
    }
}

<?php
namespace frontend\modules\company\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use application\modules\company_customers\models\CompanyCustomers;
use application\modules\company_customers\components\CustomerHelper;

class CompanyLogSearch extends Model
{
    public $id;
    public $email;
    public $date;
    public $hours;
    public $is_manual;
    
    public function rules()
    {
        return [
            [['id', 'email', 'hours', 'is_manual', 'date'], 'safe'],
            [['hours','is_manual'], 'integer']
        ];
    }

    public function formName() {
        return '';
    }

    public function attributeLabels() {
        return [

        ];
    }

    public function search($companyId, $params)
    {
        $query = Tracker::find()
            ->joinWith(['user'])
            ->andWhere(['tracker.company_id' => $companyId])
            ->orderBy(['date' => SORT_DESC, 'hours' => SORT_ASC]);

        if ($this->load($params) && $this->validate()) {
            if($this->hours > 0) {
                $query->andFilterWhere(['hours' => $this->hours]);
            }

            if(in_array($this->is_manual, [0,1])) {
                $query->andFilterWhere(['is_manual' => $this->is_manual]);
            }

            if(!empty($this->date)) {
                $query->andFilterWhere(['date' => date("Y-m-d", strtotime($this->date))]);
            }

            if(!empty($this->email)) {
                $query->andFilterWhere(['user.email' => $this->email]);
            }
        }

        // Get average
        $query2 = clone $query;
        $avgHours = $query2->select("AVG(hours) as avg_hours")->asArray()->one();
        $avgHours = round($avgHours['avg_hours'], 1);
        unset($query2);

        $query3 = clone $query;
        $totalHours = $query3->select("SUM(hours) as total_hours")->asArray()->one();
        $totalHours = round($totalHours['total_hours'], 1);
        unset($query3);

        // Get average per day
        $query3 = clone $query;
        $avgPerDayQuery = $query3->select("date, AVG(hours) as avg_hours")->groupBy(['date']);
        unset($query3);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'hours',
                    'is_manual',
                    'date',
                    'user.email'
                ],
            ]
        ]);

        return ['data' => $dataProvider, 'avg_hours' => $avgHours, 'total_hours' => $totalHours, 'avg_per_day' => $avgPerDayQuery];
    }
} 

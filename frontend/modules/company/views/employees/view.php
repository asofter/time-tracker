<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;
use frontend\modules\company\models\TrackerLog;
use \fruppel\googlecharts\GoogleCharts;

$this->title = $user['firstname'] . ' ' . $user['lastname'];
?>

<h1><?= $this->title; ?></h1>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Details</div>

            <table class="table">
                <tr>
                    <th>Id</th>
                    <td><?= $user['id']; ?></td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td><?= $user['firstname'] . ' ' . $user['lastname']; ?></td>
                </tr>
                <tr>
                    <th>E-mail</th>
                    <td><?= $user['email']; ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<p>&nbsp;</p>

<?php if(count($tracker) > 0): ?>
    <div class="panel panel-default">
        <div class="panel-heading">Log</div>
        <div class="panel-body">
            <strong>Average hours/day:</strong> <span class="badge"><?= $avgHours; ?></span><br />
            <strong>Total hours:</strong> <span class="badge"><?= $totalHours; ?></span>
            <p>&nbsp;</p>
            <?php
            $chartData = [['Date', 'Hours']];
            foreach($avgPerDay as $avg) {
                $chartData[] = [date("d.m.Y", strtotime($avg['date'])), round($avg['avg_hours'], 1)];
            }

            echo GoogleCharts::widget([
                'visualization' => 'AreaChart',
                'options' => [
                    'title' => 'Statistics',
                    'hAxis' => [
                        'title' => 'Date',
                        'titleTextStyle' => [
                            'color' => '#333'
                        ]
                    ],
                    'vAxis' => [
                        'minValue' => 0
                    ]
                ],
                'dataArray' => $chartData
            ]);
            ?>
        </div>
        <table class="table">
            <tr>
                <th>Date</th>
                <th>Hours</th>
                <th>Manual?</th>
                <th>Description</th>
                <th></th>
            </tr>
            <?php foreach($tracker as $item): ?>
                <tr>
                    <td><span class="label label-success"><?= date("d.m.Y", strtotime($item['date'])); ?></span></td>
                    <td><?= $item['hours']; ?></td>
                    <td><?= $item['is_manual'] ? 'Yes' : 'No'; ?></td>
                    <td><?= nl2br($item['description']); ?></td>
                    <td>
                        <?= Html::a('<i class="fa fa-eye"></i>',['/company/default/view', 'id' => $item['id']], ['class' => 'btn btn-primary pull-right']); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
<?php else: ?>
    <div class="alert alert-warning">No records in tracker.</div>
<?php endif; ?>



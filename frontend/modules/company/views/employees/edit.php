<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;

$this->title = "Employee Details";
?>

<div class="row">
    <div class="col-lg-9 col-md-11 col-sm-12">
        <?php $form = ActiveForm::begin(['id' => 'employees-form', 'options' => ['role' => 'form'], 'enableAjaxValidation' => true]); ?>
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $form->field($model, 'user_id', ['template' => '{input}'])->hiddenInput(['id' => 'user-id', 'style' => 'display:none;']) ?>
                <div class="form-group">
                    <label class="control-label">Email</label>
                    <?php
                    echo \yii\jui\AutoComplete::widget([
                        'model' => $model,
                        'attribute' => 'email',
                        'clientOptions' => [
                            'source' => Url::to(['/company/employees/get-user']),
                            'select' => new \yii\web\JsExpression("function(event, ui) { userSelected(ui.item); }"),
                            'change' => new \yii\web\JsExpression("function(event, ui) { clearSelect(this); }"),
                            'minLength' => 0
                        ],
                        'options' => [
                            'class' => 'form-control input-lg userSelect',
                            'style' => '',
                        ]
                    ]);
                    ?>
                    <?= $form->field($model, 'email', ['template' => '{hint}{error}']); ?>
                </div>

                <div class="non-user">
                    <?= $form->field($model, 'firstname')->textInput(['class' => 'form-control input-lg user-firstname']) ?>
                    <?= $form->field($model, 'lastname')->textInput(['class' => 'form-control input-lg user-lastname']) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-arrow-right"></i> Submit', ['class' => 'btn btn-primary btn-lg']); ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
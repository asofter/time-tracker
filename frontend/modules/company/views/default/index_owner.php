<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\User;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Url;
use yii\helpers\Json;
use \yii\grid\GridView;
use yii\grid\ActionColumn;
use \yii\widgets\Pjax;
use \fruppel\googlecharts\GoogleCharts;

$this->title = "Company Dashboard";
?>

    <strong>Average hours/day:</strong> <span class="badge"><?= $avgHours; ?></span><br />
    <strong>Total hours:</strong> <span class="badge"><?= $totalHours; ?></span><br /><br />

<?php
if($dataProvider->count > 0) {
    $chartData = [['Date', 'Hours']];
    foreach($avgPerDay as $avg) {
        $chartData[] = [date("d.m.Y", strtotime($avg['date'])), round($avg['avg_hours'], 1)];
    }

    echo GoogleCharts::widget([
        'visualization' => 'AreaChart',
        'options' => [
            'title' => 'Statistics',
            'hAxis' => [
                'title' => 'Date',
                'titleTextStyle' => [
                    'color' => '#333'
                ]
            ],
            'vAxis' => [
                'minValue' => 0
            ]
        ],
        'dataArray' => $chartData
    ]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items} {pager}',
        'filterModel' => $filterModel,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        'columns' => [
            [
                'attribute' => 'date',
                'filter' => \yii\jui\DatePicker::widget([
                    'attribute' => 'date',
                    'model' => $filterModel,
                    'options' => ['class' => 'form-control'],
                    'dateFormat' => 'dd.MM.yyyy'
                ]),
                'headerOptions' => ['style' => 'width: 150px;'],
                'value' => function($model) {
                    return date("d.m.Y", strtotime($model['date']));
                },
            ],
            [
                'attribute' => 'user.email',
                'filter' => Html::activeTextInput($filterModel, 'email', ['class' => 'form-control']),
                'label' => 'Email',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->user['email'], ['/company/employees/view', 'id' => $model['user_id']]);
                }
            ],
            [
                'attribute' => 'hours',
                'filter' => range(0, 23),
                'label' => 'Hours'
            ],
            [
                'attribute' => 'is_manual',
                'label' => 'Manual?',
                'filter' => [0 => 'No', 1 => 'Yes'],
                'value' => function($model) {
                    return $model['is_manual'] ? 'Yes' : 'No';
                }
            ],
            [
                'attribute' => 'description',
                'label' => 'Description',
                'format' => 'html',
                'value' => function($model) {
                    return nl2br($model['description']);
                },
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{view}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to(['/company/default/' . $action, 'id' => $model->id]);
                },
                'contentOptions' => ['style' => 'text-align: right;'],
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-info']);
                    }
                ]
            ]
        ]
    ]);
    ?>
<?php } else { ?>
    <div class="alert alert-warning">There are no logs that fit your search parameters.</div>
<?php } ?>
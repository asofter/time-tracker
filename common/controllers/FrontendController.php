<?php
namespace common\controllers;

use backend\modules\currency\components\CurrencyHelper;
use common\components\CacheHelper;
use common\components\LanguageHelper;
use frontend\modules\company\models\Companies;
use frontend\modules\tours\components\TourHelper;
use Yii;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\Controller;
use frontend\modules\tours\models\TourCountries;
use frontend\modules\tours\models\TourCities;

class FrontendController extends Controller
{
    public $company_id;
    public $company;

    public function beforeAction($action) {
        if(!Yii::$app->getUser()->isGuest) {
            $company = Companies::find()
                ->where(['status' => [Companies::STATUS_ACTIVE, Companies::STATUS_DISABLED], 'user_id' => Yii::$app->getUser()->id])
                ->one();
            if(!$company) {
                $company = Companies::find()
                    ->joinWith(['employees' => function ($query) { $query->andWhere(['company_employees.user_id' => Yii::$app->user->id]); }])
                    ->where(['status' => [Companies::STATUS_ACTIVE, Companies::STATUS_DISABLED]])
                    ->one();
            }
            if($company) {
                $this->company = $company;
                $this->company_id = $company['id'];
            }
        }

        if(!empty($_REQUEST['QUERY_STRING']) || preg_match('#account/#', Yii::$app->getRequest()->getPathInfo())) {
            $this->view->registerMetaTag(['name' => 'robots', 'content' =>  'noindex, follow']);
        }

        $this->view->registerLinkTag(['rel' => 'canonical', 'href' => Yii::$app->urlManager->createAbsoluteUrl(['/' . Yii::$app->getRequest()->getPathInfo()])], 'canonical');

        return parent::beforeAction($action);
    }

}

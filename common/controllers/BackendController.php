<?php
namespace common\controllers;

use common\components\LanguageHelper;
use common\models\UserRoles;
use Yii;
use yii\web\Controller;

class BackendController extends Controller
{
    public function beforeAction($action) {
        $isAdmin = false;
        if(!Yii::$app->getUser()->isGuest && in_array(Yii::$app->getUser()->getIdentity()->role, UserRoles::getAdminRoles())) {
            $isAdmin = true;
        }
        Yii::$app->params['isAdmin'] = $isAdmin;

        return parent::beforeAction($action);
    }
}

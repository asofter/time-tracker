<?php
$config = [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=tracker',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ]
    ],
    'params' => [

    ]
];

return $config;

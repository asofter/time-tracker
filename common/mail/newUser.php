<?php
use yii\helpers\Html;
use \common\components\EmailAuthHelper;

$loginLink = Yii::$app->params['main']['frontendUrl'] . 'account/default/login/';
?>
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    Hello,
</div>

<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: bold; margin: 0 0 10px; padding: 0;">
    Welcome on Tracker! You were added as an employee of the company. Please, activate your account and change your password.
</div>
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <hr style="border: 0;border-bottom: 1px solid #f0f0f0;margin-top:15px;margin-bottom:15px;">
    Your temporary password is: <strong><?= $password; ?></strong>
</div>

<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <hr style="border: 0;border-bottom: 1px solid #f0f0f0;margin-top:15px;margin-bottom:15px;">
    To activate your account, please, go to the website, enter your temporary password and change it.
    <br /><br />
    <?= Html::a(Html::encode($loginLink), $loginLink); ?>
</div>
<div style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    If you cannot click the link, please try pasting the text into your browser.
</div>
